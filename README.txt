Installation
============
1 Install JavaScript libraries:
  a. chess.js
     Go to https://github.com/jhlywa/chess.js,
     Download (https://github.com/jhlywa/chess.js/archive/master.zip) to /sites/all/libraries,
     Rename the folder from chess.js-master to chess.js
  b. chessboard.js
     Go to http://chessboardjs.com,
     Download (http://chessboardjs.com/releases/0.3.0/chessboardjs-0.3.0.zip) to /sites/all/libraries,
     Rename the folder from chessboardjs-0.3.0 to chessboard.js
  Note: install and enable 'Libraries' module if not yet.

2 Install and enable the module as normal

3 How to use
  Add as normal files (/file/add), or
  Insert as a media file in CKEditor (The 'media' button is enabled in 'Full HTML' filter by default)
